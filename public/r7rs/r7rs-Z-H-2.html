<!DOCTYPE html>
<html lang=en>
<!--
Generated from r7rs.tex by tex2page, v. 20221218
Copyright (C) 1997-2022 Dorai Sitaram
(running on ECL 21.2.1 Linux)
http://ds26gte.github.io/tex2page/index.html
-->
<head>
<meta charset="utf-8">
<title>
Revised^7 Report on the Algorithmic Language Scheme
</title>
<link rel="stylesheet" href="r7rs-Z-S.css" />
<meta name=robots content="index,follow">
</head>
<body>
<div>
<div class=navigation>[Go to <span><a class=hrefinternal href="r7rs.html">first</a>, <a class=hrefinternal href="r7rs-Z-H-1.html">previous</a></span><span>, <a class=hrefinternal href="r7rs-Z-H-3.html">next</a></span> page<span>; &#xa0;&#xa0;</span><span><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc">contents</a></span><span><span>; &#xa0;&#xa0;</span><a class=hrefinternal href="r7rs.html#TAG:__tex2page_index_start">index</a></span>]</div>
<p>
</p>
<a id="TAG:__tex2page_chap_Temp_3"></a>
<h1 class=chapter>
<div class=chapterheading>&#xa0;</div><br>
<a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_chap_Temp_3">Introduction</a></h1>
<p class=noindent>
Programming languages should be designed not by piling feature on top of
feature, but by removing the weaknesses and restrictions that make additional
features appear necessary.  Scheme demonstrates that a very small number
of rules for forming expressions, with no restrictions on how they are
composed, suffice to form a practical and efficient programming language
that is flexible enough to support most of the major programming
paradigms in use today.</p>
<p>

Scheme
was one of the first programming languages to incorporate first-class
procedures as in the lambda calculus, thereby proving the usefulness of
static scope rules and block structure in a dynamically typed language.
Scheme was the first major dialect of Lisp to distinguish procedures
from lambda expressions and symbols, to use a single lexical
environment for all variables, and to evaluate the operator position
of a procedure call in the same way as an operand position.  By relying
entirely on procedure calls to express iteration, Scheme emphasized the
fact that tail-recursive procedure calls are essentially GOTOs that
pass arguments, thus allowing a programming style that is both coherent
and efficient.  Scheme was the first widely used programming language to
embrace first-class escape procedures, from which all previously known
sequential control structures can be synthesized.  A subsequent
version of Scheme introduced the concept of exact and inexact numbers,
an extension of Common Lisp&#x2019;s generic arithmetic.
More recently, Scheme became the first programming language to support
hygienic macros, which permit the syntax of a block-structured language
to be extended in a consistent and reliable manner.
</p>
<a id="TAG:__tex2page_sec_Temp_4"></a>
<h3 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_Temp_4">Background</a></h3>
<p class=noindent>The first description of Scheme was written in
1975&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_35">35</a>].  A revised report&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_31">31</a>]
appeared in 1978, which described the evolution
of the language as its MIT implementation was upgraded to support an
innovative compiler&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_32">32</a>].  Three distinct projects began in
1981 and 1982 to use variants of Scheme for courses at MIT, Yale, and
Indiana University&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_27">27</a>,&#xa0;<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_24">24</a>,&#xa0;<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_14">14</a>].  An introductory
computer science textbook using Scheme was published in
1984&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_1">1</a>].</p>
<p>

As Scheme became more widespread,
local dialects began to diverge until students and researchers
occasionally found it difficult to understand code written at other
sites.
Fifteen representatives of the major implementations of Scheme therefore
met in October 1984 to work toward a better and more widely accepted
standard for Scheme.
Their report, the RRRS&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_8">8</a>],
was published at MIT and Indiana University in the summer of 1985.
Further revision took place in the spring of 1986, resulting in the
R<sup>3</sup>RS&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_29">29</a>].
Work in the spring of 1988 resulted in R<sup>4</sup>RS&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_10">10</a>],
which became the basis for the
IEEE Standard for the Scheme Programming Language in 1991&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_18">18</a>].
In 1998, several additions to the IEEE standard, including high-level
hygienic macros, multiple return values, and <span style="font-family: monospace">eval</span>, were finalized
as the R<sup>5</sup>RS&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_20">20</a>].</p>
<p>
In the fall of 2006, work began on a more ambitious standard,
including many new improvements and stricter requirements made in the
interest of improved portability.  The resulting standard, the
R<sup>6</sup>RS, was completed in August 2007&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_33">33</a>], and was organized
as a core language and set of mandatory standard libraries.  
Several new implementations of Scheme conforming to it were created.
However, most existing R<sup>5</sup>RS implementations (even excluding those
which are essentially unmaintained) did not adopt R<sup>6</sup>RS, or adopted
only selected parts of it.</p>
<p>

In consequence, the Scheme Steering Committee decided in August 2009 to divide the
standard into two separate but compatible languages &#x2014; a &#x201c;small&#x201d;
language, suitable for educators, researchers, and users of embedded languages,
focused on R<sup>5</sup>RS&#xa0;compatibility, and a &#x201c;large&#x201d; language focused
on the practical needs of mainstream software development,
intended to become a replacement for R<sup>6</sup>RS.
The present report describes the &#x201c;small&#x201d; language of that effort:
therefore it cannot be considered in isolation as the successor
to R<sup>6</sup>RS.</p>
<p>

</p>
<div class=medskip></div>
<p style="margin-top: 0pt; margin-bottom: 0pt">
We intend this report to belong to the entire Scheme community, and so
we grant permission to copy it in whole or in part without fee.  In
particular, we encourage implementers of Scheme to use this report as
a starting point for manuals and other documentation, modifying it as
necessary.</p>
<p>

</p>
<a id="TAG:__tex2page_sec_Temp_5"></a>
<h3 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_Temp_5">Acknowledgments</a></h3>
<p class=noindent>We would like to thank the members of the Steering Committee, William
Clinger, Marc Feeley, Chris Hanson, Jonathan Rees, and Olin Shivers, for
their support and guidance.</p>
<p>

This report is very much a community effort, and we&#x2019;d like to
thank everyone who provided comments and feedback, including
the following people: David Adler, Eli Barzilay, Taylan Ulrich
Bay&#x131;rl&#x131;/Kammer, Marco Benelli, Pierpaolo Bernardi,
Peter Bex, Per Bothner, John Boyle, Taylor Campbell, Raffael Cavallaro,
Ray Dillinger, Biep Durieux, Sztefan Edwards, Helmut Eller, Justin
Ethier, Jay Reynolds Freeman, Tony Garnock-Jones, Alan Manuel Gloria,
Steve Hafner, Sven Hartrumpf, Brian Harvey, Moritz Heidkamp, Jean-Michel
Hufflen, Aubrey Jaffer, Takashi Kato, Shiro Kawai, Richard Kelsey, Oleg
Kiselyov, Pjotr Kourzanov, Jonathan Kraut, Daniel Krueger, Christian
Stigen Larsen, Noah Lavine, Stephen Leach, Larry D. Lee, Kun Liang,
Thomas Lord, Vincent Stewart Manis, Perry Metzger, Michael Montague,
Mikael More, Vitaly Magerya, Vincent Manis, Vassil Nikolov, Joseph
Wayne Norton, Yuki Okumura, Daichi Oohashi, Jeronimo Pellegrini, Jussi
Piitulainen, Alex Queiroz, Jim Rees, Grant Rettke, Andrew Robbins, Devon
Schudy, Bakul Shah, Robert Smith, Arthur Smyles, Michael Sperber, John
David Stone, Jay Sulzberger, Malcolm Tredinnick, Sam Tobin-Hochstadt,
Andre van Tonder, Daniel Villeneuve, Denis Washington, Alan Watson,
Mark H.  Weaver, Go&#x308;ran Weinholt, David A. Wheeler, Andy Wingo, James
Wise, Jo&#x308;rg F. Wittenberger, Kevin A. Wortman, Sascha Ziemann.</p>
<p>

In addition we would like to thank all the past editors, and the
people who helped them in turn: Hal Abelson, Norman Adams, David
Bartley, Alan Bawden, Michael Blair, Gary Brooks, George Carrette,
Andy Cromarty, Pavel Curtis, Jeff Dalton, Olivier Danvy, Betty Dexter, Ken Dickey,
Bruce Duba, Robert Findler, Andy Freeman, Richard Gabriel, Yekta
Gu&#x308;rsel, Ken Haase, Robert Halstead, Robert Hieb, Paul Hudak, Morry
Katz, Eugene Kohlbecker, Chris Lindblad, Jacob Matthews, Mark Meyer,
Jim Miller, Don Oxley, Jim Philbin, Kent Pitman, John Ramsdell,
Guillermo Rozas, Mike Shaff, Jonathan Shapiro, Guy Steele, Julie
Sussman, Perry Wagle, Mitchel Wand, Daniel Weise, Henry Wu, and Ozan
Yigit.  We thank Carol Fessenden, Daniel Friedman, and Christopher
Haynes for permission to use text from the Scheme 311 version 4
reference manual.  We thank Texas Instruments, Inc.&#xa0;for permission to
use text from the <em>TI Scheme Language Reference
Manual</em>&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_37">37</a>].  We gladly acknowledge the influence of
manuals for MIT Scheme&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_24">24</a>], T&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_28">28</a>], Scheme
84&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_15">15</a>], Common Lisp&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_34">34</a>], and Algol 60&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_25">25</a>],
as well as the following SRFIs:  0, 1, 4, 6, 9, 11, 13, 16, 30, 34, 39, 43, 46, 62, and 87,
all of which are available at <span style="font-family: monospace">http://srfi.schemers.org</span>.</p>
<p>
<div style="height: 14.4pt"></div>
<p style="margin-top: 0pt; margin-bottom: 0pt">
</p>
<div class=smallskip></div>
<p style="margin-top: 0pt; margin-bottom: 0pt">
</p>
<div class=navigation>[Go to <span><a class=hrefinternal href="r7rs.html">first</a>, <a class=hrefinternal href="r7rs-Z-H-1.html">previous</a></span><span>, <a class=hrefinternal href="r7rs-Z-H-3.html">next</a></span> page<span>; &#xa0;&#xa0;</span><span><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc">contents</a></span><span><span>; &#xa0;&#xa0;</span><a class=hrefinternal href="r7rs.html#TAG:__tex2page_index_start">index</a></span>]</div>
<p>
</p>
</div>
</body>
</html>

