<!DOCTYPE html>
<html lang=en>
<!--
Generated from r7rs.tex by tex2page, v. 20221218
Copyright (C) 1997-2022 Dorai Sitaram
(running on ECL 21.2.1 Linux)
http://ds26gte.github.io/tex2page/index.html
-->
<head>
<meta charset="utf-8">
<title>
Revised^7 Report on the Algorithmic Language Scheme
</title>
<link rel="stylesheet" href="r7rs-Z-S.css" />
<meta name=robots content="index,follow">
</head>
<body>
<div>
<div class=navigation>[Go to <span><a class=hrefinternal href="r7rs.html">first</a>, <a class=hrefinternal href="r7rs-Z-H-3.html">previous</a></span><span>, <a class=hrefinternal href="r7rs-Z-H-5.html">next</a></span> page<span>; &#xa0;&#xa0;</span><span><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc">contents</a></span><span><span>; &#xa0;&#xa0;</span><a class=hrefinternal href="r7rs.html#TAG:__tex2page_index_start">index</a></span>]</div>
<p>
</p>
<a id="TAG:__tex2page_chap_2"></a>
<h1 class=chapter>
<div class=chapterheading><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_chap_2">Chapter 2</a></div><br>
<a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_chap_2">Lexical conventions</a></h1>
<p class=noindent>This section gives an informal account of some of the lexical
conventions used in writing Scheme programs.  For a formal syntax of
Scheme, see section&#xa0;<a class=hrefinternal href="r7rs-Z-H-9.html#TAG:__tex2page_sec_7.1">7.1</a>.</p>
<p>

</p>
<a id="TAG:__tex2page_sec_2.1"></a>
<h2 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_2.1">2.1&#xa0;&#xa0;Identifiers</a></h2>
<p class=noindent></p>
<p>

An identifier<a id="TAG:__tex2page_index_22"></a>is any sequence of letters, digits, and
&#x201c;extended identifier characters&#x201d; provided that it does not have a prefix
which is a valid number.  
However, the  <span style="font-family: monospace">.</span> token (a single period) used in the list syntax
is not an identifier.</p>
<p>

All implementations of Scheme must support the following extended identifier
characters:</p>
<p>

</p>
<span style="font-family: monospace">!&#x200b; &#x200b;$&#xa0;%&#xa0;<code class=verbatim>&#x26;</code>&#xa0;*&#xa0;+&#xa0;-&#xa0;.&#xa0;/&#xa0;:&#x200b; &#x200b;&#x3c;&#xa0;=&#xa0;&#x3e;&#xa0;?&#xa0;@&#xa0;<code class=verbatim>^</code>&#xa0;<code class=verbatim>_</code>&#xa0;<code class=verbatim>~</code>&#xa0;</span>
Alternatively, an identifier can be represented by a sequence of zero or more
characters enclosed within vertical lines (<span style="font-family: monospace">|</span>), analogous to
string literals.  Any character, including whitespace characters, but
excluding the backslash and vertical line characters,
can appear verbatim in such an identifier.
In addition, characters can be
specified using either an &#x3c;inline hex escape&#x3e; or
the same escapes
available in strings.<p>

For example, the
identifier <code class=verbatim>|H\x65;llo|</code> is the same identifier as
<code class=verbatim>Hello</code>, and in an implementation that supports the appropriate
Unicode character the identifier <code class=verbatim>|\x3BB;|</code> is the same as the
identifier &#x3bb;.
What is more, <code class=verbatim>|\t\t|</code> and <code class=verbatim>|\x9;\x9;|</code> are the
same.
Note that <code class=verbatim>||</code> is a valid identifier that is different from any other
identifier.</p>
<p>

Here are some examples of identifiers:</p>
<p>

</p>
<span style="font-family: monospace">...&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;+<br>
+soup+&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#x3c;=?<br>
-&#x3e;string&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;a34kTMNs<br>
lambda&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;list-&#x3e;vector<br>
q&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;V17a<br>
|two&#xa0;words|&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;|two<span style="font-family: monospace">\</span>x20;words|<br>
the-word-recursion-has-many-meanings</span>
See section&#xa0;<a class=hrefinternal href="r7rs-Z-H-9.html#TAG:__tex2page_sec_7.1.1">7.1.1</a> for the formal syntax of identifiers.<p>

Identifiers have two uses within Scheme programs:
</p>
<ul>

<li><p>
Any identifier can be used as a variable<a id="TAG:__tex2page_index_24"></a>or as a syntactic keyword<a id="TAG:__tex2page_index_26"></a>(see sections&#xa0;<a class=hrefinternal href="r7rs-Z-H-5.html#TAG:__tex2page_sec_3.1">3.1</a> and&#xa0;<a class=hrefinternal href="r7rs-Z-H-6.html#TAG:__tex2page_sec_4.3">4.3</a>).</p>
<p>

</p>
<li><p>
When an identifier appears as a literal or within a literal
(see section&#xa0;<a class=hrefinternal href="r7rs-Z-H-6.html#TAG:__tex2page_sec_4.1.2">4.1.2</a>), it is being used to denote a <em>symbol</em>
(see section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.5">6.5</a>).
</p>
</ul>
<p class=noindent>
In contrast with earlier revisions of the report&#xa0;[<a class=hrefinternal href="r7rs-Z-H-15.html#TAG:__tex2page_bib_20">20</a>], the
syntax distinguishes between upper and lower case in
identifiers and in characters specified using their names.  However, it
does not distinguish between upper and lower case in numbers,
nor in &#x3c;inline hex escapes&#x3e; used
in the syntax of identifiers, characters, or strings.
None of the identifiers defined in this report contain upper-case
characters, even when they appear to do so as a result
of the English-language convention of capitalizing the first word of
a sentence.</p>
<p>

The following directives give explicit control over case
folding.</p>
<p>

</p>
<p>
<span style="font-family: monospace">#!fold-case</span><a id="TAG:__tex2page_index_28"></a><br>
<span style="font-family: monospace">#!no-fold-case</span><a id="TAG:__tex2page_index_30"></a></p>
<p>

These directives can appear anywhere comments are permitted (see
section&#xa0;<a class=hrefinternal href="#TAG:__tex2page_sec_2.2">2.2</a>) but must be followed by a delimiter.
They are treated as comments, except that they affect the reading
of subsequent data from the same port. The <span style="font-family: monospace">#!fold-case</span> directive causes
subsequent identifiers and character names to be case-folded
as if by <span style="font-family: monospace">string-foldcase</span> (see section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.7">6.7</a>).
It has no effect on character
literals.  The <span style="font-family: monospace">#!no-fold-case</span> directive
causes a return to the default, non-folding behavior.
</p>
<p>
</p>
<a id="TAG:__tex2page_sec_2.2"></a>
<h2 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_2.2">2.2&#xa0;&#xa0;Whitespace and comments</a></h2>
<p class=noindent></p>
<p>

<a id="TAG:__tex2page_index_32"></a><em>Whitespace</em> characters include the space, tab, and newline characters.
(Implementations may provide additional whitespace characters such
as page break.)  Whitespace is used for improved readability and
as necessary to separate tokens from each other, a token being an
indivisible lexical unit such as an identifier or number, but is
otherwise insignificant.  Whitespace can occur between any two tokens,
but not within a token.  Whitespace occurring inside a string
or inside a symbol delimited by vertical lines
is significant.</p>
<p>

The lexical syntax includes several comment forms.  
Comments are treated exactly like whitespace.</p>
<p>

A semicolon (<span style="font-family: monospace">;</span>) indicates the start of a line
comment.<a id="TAG:__tex2page_index_34"></a><a id="TAG:__tex2page_index_36"></a>The comment continues to the
end of the line on which the semicolon appears.
Another way to indicate a comment is to prefix a &#x3c;datum&#x3e;
(cf.&#x200b; &#x200b;section&#xa0;<a class=hrefinternal href="r7rs-Z-H-9.html#TAG:__tex2page_sec_7.1.2">7.1.2</a>) with <span style="font-family: monospace">#;</span><a id="TAG:__tex2page_index_38"></a>and optional
&#x3c;whitespace&#x3e;.  The comment consists of
the comment prefix <span style="font-family: monospace">#;</span>, the space, and the &#x3c;datum&#x3e; together.  This
notation is useful for &#x201c;commenting out&#x201d; sections of code.</p>
<p>

Block comments are indicated with properly nested <span style="font-family: monospace">
#|</span><a id="TAG:__tex2page_index_40"></a><a id="TAG:__tex2page_index_42"></a>and <span style="font-family: monospace">|#</span> pairs.</p>
<p>

</p>
<span style="font-family: monospace">#|<br>
&#xa0;&#xa0;&#xa0;The&#xa0;FACT&#xa0;procedure&#xa0;computes&#xa0;the&#xa0;factorial<br>
&#xa0;&#xa0;&#xa0;of&#xa0;a&#xa0;non-negative&#xa0;integer.<br>
|#<br>
(define&#xa0;fact<br>
&#xa0;&#xa0;(lambda&#xa0;(n)<br>
&#xa0;&#xa0;&#xa0;&#xa0;(if&#xa0;(=&#xa0;n&#xa0;0)<br>
&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;#;(=&#xa0;n&#xa0;1)<br>
&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;1&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;;Base&#xa0;case:&#xa0;return&#xa0;1<br>
&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;(*&#xa0;n&#xa0;(fact&#xa0;(-&#xa0;n&#xa0;1))))))</span><p>

</p>
<a id="TAG:__tex2page_sec_2.3"></a>
<h2 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_2.3">2.3&#xa0;&#xa0;Other notations</a></h2>
<p class=noindent></p>
<p>

For a description of the notations used for numbers, see
section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.2">6.2</a>.</p>
<p>

</p>
<dl><dt></dt><dd><p>

</p>
</dd><dt><b><span style="font-family: monospace">.&#x200b; &#x200b;+ -</span></b></dt><dd>
These are used in numbers, and can also occur anywhere in an identifier.
A delimited plus or minus sign by itself
is also an identifier.
A delimited period (not occurring within a number or identifier) is used
in the notation for pairs (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.4">6.4</a>), and to indicate a
rest-parameter in a  formal parameter list (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-6.html#TAG:__tex2page_sec_4.1.4">4.1.4</a>).
Note that a sequence of two or more periods <em>is</em> an identifier.<p>

</p>
</dd><dt><b><span style="font-family: monospace">( )</b></span></dt><dd>
Parentheses are used for grouping and to notate lists
(section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.4">6.4</a>).<p>

</p>
</dd><dt><b><span style="font-family: monospace">'</span></b></dt><dd>
The apostrophe (single quote) character is used to indicate literal data (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-6.html#TAG:__tex2page_sec_4.1.2">4.1.2</a>).<p>

</p>
</dd><dt><b><span style="font-family: monospace">`</span></b></dt><dd>
The grave accent (backquote) character is used to indicate partly constant
data (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-6.html#TAG:__tex2page_sec_4.2.8">4.2.8</a>).<p>

</p>
</dd><dt><b><span style="font-family: monospace">, ,@</b></span></dt><dd>
The character comma and the sequence comma at-sign are used in conjunction
with quasiquotation (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-6.html#TAG:__tex2page_sec_4.2.8">4.2.8</a>).<p>

</p>
</dd><dt><b><span style="font-family: monospace">&#x22;</b></span></dt><dd>
The quotation mark character is used to delimit strings (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.7">6.7</a>).<p>

</p>
</dd><dt><b><span style="font-family: monospace">\</span></b></dt><dd>
Backslash is used in the syntax for character constants
(section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.6">6.6</a>) and as an escape character within string
constants (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.7">6.7</a>) and identifiers
(section&#xa0;<a class=hrefinternal href="r7rs-Z-H-9.html#TAG:__tex2page_sec_7.1.1">7.1.1</a>).<p>

</p>
</dd><dt><b><code class=verbatim>[ ] { } |</code></b></dt><dd>
Left and right square and curly brackets (braces)
are reserved for possible future extensions to the language.<p>

</p>
</dd><dt><b><span style="font-family: monospace">#</span></b></dt><dd> The number sign is used for a variety of purposes depending on
the character that immediately follows it:<p>

</p>
</dd><dt><b><span style="font-family: monospace">#t</span> <span style="font-family: monospace">#f</span></b></dt><dd>
These are the boolean constants (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.3">6.3</a>),
along with the alternatives <span style="font-family: monospace">#true</span> and <span style="font-family: monospace">#false</span>.<p>

</p>
</dd><dt><b><span style="font-family: monospace">#</span><span style="font-family: monospace">\</span></b></dt><dd>
This introduces a character constant (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.6">6.6</a>).<p>

</p>
</dd><dt><b><span style="font-family: monospace">#</span><span style="font-family: monospace">(</b></span></dt><dd>
This introduces a vector constant (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.8">6.8</a>).  Vector constants
are terminated by&#xa0;<span style="font-family: monospace">)</span>&#xa0;.<p>

</p>
</dd><dt><b><span style="font-family: monospace">#</span><span style="font-family: monospace">u8(</b></span></dt><dd>
This introduces a bytevector constant (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.9">6.9</a>).  Bytevector constants
are terminated by&#xa0;<span style="font-family: monospace">)</span>&#xa0;.<p>

</p>
</dd><dt><b><span style="font-family: monospace">#e #i #b #o #d #x</span></b></dt><dd>
These are used in the notation for numbers (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.2.5">6.2.5</a>).<p>

</p>
</dd><dt><b><span style="font-family: monospace">#&#x3c;n&#x3e;= #&#x3c;n&#x3e;#</b></span></dt><dd>
These are used for labeling and referencing other literal data (section&#xa0;<a class=hrefinternal href="#TAG:__tex2page_sec_2.4">2.4</a>).<p>

</p>
</dd></dl>
<p class=noindent>
</p>
<a id="TAG:__tex2page_sec_2.4"></a>
<h2 class=section><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc_TAG:__tex2page_sec_2.4">2.4&#xa0;&#xa0;Datum labels</a></h2>
<p class=noindent><div style="height: -14.4pt"></div>
<p style="margin-top: 0pt; margin-bottom: 0pt">
</p>
<p>
</p>
<div class=leftline><u>lexical syntax:</u><span style="margin-left: .5em"> </span><span style="font-family: monospace">#&#x3c;n&#x3e;=&#x3c;datum&#x3e;</span>&#xa0;</div>

<div class=leftline><u>lexical syntax:</u><span style="margin-left: .5em"> </span><span style="font-family: monospace">#&#x3c;n&#x3e;#</span>&#xa0;</div>
<p>

The lexical syntax
<span style="font-family: monospace">#&#x3c;n&#x3e;=&#x3c;datum&#x3e;</span> reads the same as &#x3c;datum&#x3e;, but also
results in &#x3c;datum&#x3e; being labelled by &#x3c;n&#x3e;.
It is an error if &#x3c;n&#x3e; is not a sequence of digits.</p>
<p>

The lexical syntax <span style="font-family: monospace">#&#x3c;n&#x3e;#</span> serves as a reference to some
object labelled by <span style="font-family: monospace">#&#x3c;n&#x3e;=</span>; the result is the same
object as the <span style="font-family: monospace">#&#x3c;n&#x3e;</span>= 
(see section&#xa0;<a class=hrefinternal href="r7rs-Z-H-8.html#TAG:__tex2page_sec_6.1">6.1</a>).
Together, these syntaxes permit the notation of
structures with shared or circular substructure.</p>
<p>

</p>
<span style="font-family: monospace">(let&#xa0;((x&#xa0;(list&#xa0;'a&#xa0;'b&#xa0;'c)))<br>
&#xa0;&#xa0;(set-cdr!&#xa0;(cddr&#xa0;x)&#xa0;x)<br>
&#xa0;&#xa0;x)&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;<span style="margin-left: 2em"> </span>&#x27f9;&#xa0;#0=(a&#xa0;b&#xa0;c&#xa0;.&#xa0;#0#)<br>
</span>
The scope of a datum label is the portion of the outermost datum in which it appears
that is to the right of the label.
Consequently, a reference <span style="font-family: monospace">#&#x3c;n&#x3e;#</span> can occur only after a label
<span style="font-family: monospace">#&#x3c;n&#x3e;=</span>; it is an error to attempt a forward reference.  In
addition, it is an error if the reference appears as the labelled object itself
(as in <span style="font-family: monospace">#&#x3c;n&#x3e;= #&#x3c;n&#x3e;#</span>),
because the object labelled by <span style="font-family: monospace">#&#x3c;n&#x3e;=</span> is not well
defined in this case.<p>

It is an error for a &#x3c;program&#x3e; or &#x3c;library&#x3e; to include
circular references except in literals.  In particular,
it is an error for <span style="font-family: monospace">quasiquote</span> (section&#xa0;<a class=hrefinternal href="r7rs-Z-H-6.html#TAG:__tex2page_sec_4.2.8">4.2.8</a>) to contain them.</p>
<p>

</p>
<span style="font-family: monospace">#1=(begin&#xa0;(display&#xa0;#<span style="font-family: monospace">\</span>x)&#xa0;#1#)<br>
&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;<span style="margin-left: 2em"> </span>&#x27f9;&#xa0;<i>error</i></span><p>
</p>
<div class=smallskip></div>
<p style="margin-top: 0pt; margin-bottom: 0pt">
</p>
<div class=navigation>[Go to <span><a class=hrefinternal href="r7rs.html">first</a>, <a class=hrefinternal href="r7rs-Z-H-3.html">previous</a></span><span>, <a class=hrefinternal href="r7rs-Z-H-5.html">next</a></span> page<span>; &#xa0;&#xa0;</span><span><a class=hrefinternal href="r7rs-Z-H-1.html#TAG:__tex2page_toc">contents</a></span><span><span>; &#xa0;&#xa0;</span><a class=hrefinternal href="r7rs.html#TAG:__tex2page_index_start">index</a></span>]</div>
<p>
</p>
</div>
</body>
</html>

